FROM python:3.7.3

COPY requirements.txt /requirements.txt
COPY requirements.dev.txt /requirements.dev.txt

RUN pip install -r requirements.txt
RUN pip install -r requirements.dev.txt

COPY . /app

WORKDIR /app

RUN pip install -e .[dev,test] && chmod +x ./production.sh

CMD ["./production.sh"]
